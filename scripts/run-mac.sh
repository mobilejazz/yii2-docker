#!/bin/bash

# Swap xdebug configs
BASE_DIR=/etc/php/7.0/mods-available

rm ${BASE_DIR}/xdebug.ini
cp ${BASE_DIR}/xdebug.mac.ini ${BASE_DIR}/xdebug.ini

# Run
exec supervisord -n