Yii2 Docker
-----------

This is a basic docker image for developing Yii2 applications. It's features include:

- PHP 7.0
- Nginx 

The default nginx config shipped with this image will use `/var/www/html/frontend/web` as it's document root.

Volumes included are:

- `/var/www/html`

Things To Note
--------------

* Everything is run under the `www-data` user with a home directory of `/var/www` and which has a uuid of `1000` to avoid permission conflicts with host file systems.
* There is a utility script located at `/exec-www.sh` to help with executing commands as the `www-data` user from the `/var/www/html` directory.
* By default the entrypoint for the image is `/run.sh`. If you are running Docker Native for Mac you will need to do the following in order to correctly use XDebug:

    - Follow the installation instructions [here](https://gist.github.com/ralphschindler/535dc5916ccbd06f53c1b0ee5a868c93) to setup a loopback alias
    - Override the entrypoint of the image in your `docker-compose.yml` file to use `/run-mac.sh`. This will swap the default `xdebug.ini` for a version configured to use the loopback alias.
